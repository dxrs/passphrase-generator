import React, { ChangeEvent } from 'react'

interface Option {
  label: string
  value: string
}

interface DropdownProps {
  options: Option[]
  onSelect: (selectedOption: string) => void
}

const Dropdown: React.FC<DropdownProps> = ({ options, onSelect }) => {
  const handleSelect = (event: ChangeEvent<HTMLSelectElement>) => {
    const selectedOption = event.target.value
    onSelect(selectedOption)
  }

  return (
    <select
      className="text-sm sm:text-base bg-white border border-gray-300 rounded px-3 py-2"
      onChange={handleSelect}
    >
      {options.map((option, index) => (
        <option key={index} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  )
}

export default Dropdown
