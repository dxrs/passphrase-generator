import PassphraseGenerator from './components/PassphraseGenerator'

function App() {
  return (
    <div className="App">
      <PassphraseGenerator />
    </div>
  )
}

export default App
